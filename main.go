package main

import "github.com/gin-gonic/gin"
import "github.com/go-sql-driver/mysql"

func main() {
	router := gin.Default()
	router.GET("/ping", func(context *gin.Context) {
		context.JSON(200, gin.H{
			"message": "pong",
		})
	})
	router.Run("localhost:4242")
}
