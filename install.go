package main

import "fmt"
import "github.com/spf13/viper"
import "bytes"
import "database/sql"
import _ "github.com/go-sql-driver/mysql"


func main() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()

	if err == nil {
		var hostdsn bytes.Buffer //  "user:password@tcp(127.0.0.1:3306)/hello"
		hostdsn.WriteString(viper.GetString("database.username"))
		hostdsn.WriteString(":")
		hostdsn.WriteString(viper.GetString("database.password"))
		hostdsn.WriteString("@tcp(")
		hostdsn.WriteString(viper.GetString("database.host"))
		hostdsn.WriteString(")/")
		db, err := sql.Open("mysql", hostdsn.String())
		if err != nil {
			panic(err.Error())
		}
		err = db.Ping()
		if err != nil {
			panic(err.Error())
		}
		fmt.Println("Success when connecting to the database, exiting")
		db.Close()
	} else {
		panic(err.Error())
	}
}
